package com.zawadal.soap.webservices.soapcoursemamagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapCourseMamagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapCourseMamagementApplication.class, args);
	}

}
